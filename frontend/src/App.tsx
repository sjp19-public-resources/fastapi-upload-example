import './App.css'
import { useState } from 'react'

interface ImageResponse {
    url: string
}

function App() {
    const [imageURL, setImageURL] = useState("")
    const [error, setError] = useState("")

    async function handleSubmit(e: React.FormEvent<HTMLFormElement>) {
        // Prevent form submission
        e.preventDefault()
        // grab the file data from the input element 
        const form = e.currentTarget
        const input = form.image as HTMLInputElement
        if (!input.files) {
            setError("No File")
            return
        }
        const file = input.files.item(0)
        if (!file) {
            setError("No File")
            return
        }
        // Build Formdata to post to the backend
        const formData = new FormData();
        // Append the file info
        formData.append('file', file)
        try {
            // POST it
            const response = await fetch("http://localhost:8000/upload", {
                method: "POST",
                body: formData
            })
            if (!response.ok) {
                console.error(response)
                throw Error("Bad response")
            }
            // Parse the response
            const data: ImageResponse = await response.json()
            // Get the newly uploaded image URL
            if (data.url) {
                setImageURL(data.url)
            }
        } catch (e) {
            if (e instanceof Error) {
                setError(e.message)
            }
            console.error(e);
        }

    }

    return (
        <>
            {error ? <p>{error}</p> : null}
            {imageURL ? <img src={imageURL} /> : null}
            <form onSubmit={handleSubmit}>
                <input name="image" type="file" />
                <button type="submit">Upload</button>
            </form>
        </>
    )
}

export default App
