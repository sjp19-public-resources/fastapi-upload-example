from fastapi import FastAPI, UploadFile
from fastapi.middleware.cors import CORSMiddleware
from rich import print
import requests
from dotenv import load_dotenv
from pydantic import BaseModel
import os

# Load API key from .env file
load_dotenv()


class Image(BaseModel):
    filename: str
    name: str
    mime: str
    extension: str
    url: str


class ImageData(BaseModel):
    id: str
    title: str
    url_viewer: str
    url: str
    display_url: str
    width: int
    height: int
    size: int
    time: int
    expiration: int
    image: Image
    thumb: Image
    medium: Image
    delete_url: str


class ImageResponse(BaseModel):
    data: ImageData
    success: bool
    status: int


app = FastAPI()

origins = ["http://localhost:5173"]

app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods="[*]",
    allow_headers="[*]",
)


@app.post("/upload")
async def upload(file: UploadFile):
    print(file)
    image = await file.read()

    response = requests.post(
        "https://api.imgbb.com/1/upload",
        params={"key": os.environ.get("API_KEY")},
        files={"image": image},
    )

    imageResponse = ImageResponse(**response.json())
    print(imageResponse)

    # This is where we could save any URLs we want to save into
    # a database

    # Return just the url we need to the UI.
    return {"url": imageResponse.data.url}
