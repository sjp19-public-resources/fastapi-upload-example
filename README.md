# Fast API Upload Example

This example uses the free service at https://api.imgbb.com/

Check the API docs for more info.

## Backend

The backend is a single file FastAPI app, in `main.py`.

You can put your API key into the .env file (see the sample file)

It has a single route used to upload an image.

You can start it with unvicorn, by using the start.sh shell script (or running uvicorn manually)

## Frontend

The frontend is a single page React app that has an upload form. You can upload an image
and it will display in the UI.

You can start the app with `npm run dev`

## Missing pieces

You will still need to implement a database to store the URLs of the images you upload.

You can login to the imgbb.com website to delete or clear out old images.


