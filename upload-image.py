import requests
from dotenv import load_dotenv
from rich import print
import os

# Load API key from .env file
load_dotenv()

# Open up the image file
file = open("cute-dog.jpg", "rb")
image = file.read()

# Post the image
response = requests.post(
    "https://api.imgbb.com/1/upload",
    params={"key": os.environ.get("API_KEY")},
    files={"image": image},
)

# Print the json response
print(response.json())
